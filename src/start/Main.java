/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package start;

import db_config.FileDbConfig;
import db_config.FDbConfig;
import db_config.IDbConfigForm;
import javax.swing.UIManager;

/**
 *
 * @author edis
 */
public class Main {

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
        }

        FileDbConfig fileDbConfig = new FileDbConfig("db-config.cfg");
        

        IDbConfigForm configForm = null;
        try {
            fileDbConfig.createDbConfigFile();
            configForm = new FDbConfig(fileDbConfig);
            configForm.showForm();
        } catch (Exception ex) {
            if (configForm != null) {
                configForm.close();
            }
            ex.printStackTrace();
        }

    }
}
