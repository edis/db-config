/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db_config;

import java.io.IOException;

/**
 *
 * @author edis
 */
public interface IDbConfig {

    public void load() throws IOException;

    public void save() throws IOException;

    public String getDbms();

    public String getHost();

    public int getPort() throws NumberFormatException;

    public String getDatabase();

    public String getUser();

    public String getPassword();

    public void setDbms(String dbms);

    public void setHost(String host);

    public void setPort(int port);

    public void setDatabase(String db);

    public void setUser(String user);

    public void setPassword(String pass);

    public String getUrl();
}
