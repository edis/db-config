package db_config;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author edis
 */
final class Constants {

    public static final String DBMS = "dbms";
    public static final String USER = "user";
    public static final String PASSWORD = "password";
    public static final String PORT = "port";
    public static final String HOST = "host";
    public static final String DATABASE = "database";
    static String FILE_NAME = "";

}
