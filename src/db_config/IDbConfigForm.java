/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db_config;

/**
 *
 * @author edis
 */
public interface IDbConfigForm {

    public void setDbConfig(IDbConfig config);

    public void update();

    public void showForm();

    public void hide();

    public void close();

}
