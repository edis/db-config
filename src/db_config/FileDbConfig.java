package db_config;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Properties;

/**
 *
 * @author edis
 */
public final class FileDbConfig implements IDbConfig {

    private Properties props;
    private String fileName;

    public FileDbConfig(String fileName) {
        props = new Properties();
        this.fileName = fileName;
        Constants.FILE_NAME = fileName;
    }

    public void load() throws IOException {
        props.load(new FileInputStream(fileName));
    }

    public void save() throws IOException {
        props.store(new BufferedWriter(new FileWriter(fileName)), "Modified: " + LocalDateTime.now().toString());
    }

    public void createDbConfigFile() throws IOException {
        File file = new File(fileName);
        if (!file.exists()) {
            Properties p = new Properties();
            p.setProperty("dbms", "mysql");
            p.setProperty("host", "localhost");
            p.setProperty("port", 3306 + "");
            p.setProperty("database", "test");
            p.setProperty("user", "root");
            p.setProperty("password", "");

            p.store(new BufferedWriter(new FileWriter(file)), "Modified: " + LocalDateTime.now().toString());
        }
    }

    public String getDbms() {
        return props.getProperty(Constants.DBMS);
    }

    public String getHost() {
        return props.getProperty(Constants.HOST);
    }

    public int getPort() throws NumberFormatException {
        return Integer.parseInt(props.getProperty(Constants.PORT + ""));
    }

    public String getDatabase() {
        return props.getProperty(Constants.DATABASE);
    }

    public String getUser() {
        return props.getProperty(Constants.USER);
    }

    public String getPassword() {
        return props.getProperty(Constants.PASSWORD);
    }

    public void setDbms(String dbms) {
        props.setProperty(Constants.DBMS, dbms);
    }

    public void setHost(String host) {
        props.setProperty(Constants.HOST, host);
    }

    public void setPort(int port) {
        props.setProperty(Constants.PORT, port + "");
    }

    public void setDatabase(String db) {
        props.setProperty(Constants.DATABASE, db);
    }

    public void setUser(String user) {
        props.setProperty(Constants.USER, user);
    }

    public void setPassword(String pass) {
        props.setProperty(Constants.PASSWORD, pass);
    }

    public String getUrl() {
        return "jdbc:" + getDbms() + "://" + getHost() + ":" + getPort() + "/" + getDatabase();
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
        Constants.FILE_NAME = fileName;
    }
}
